// on charge des modules
var http = require('http');
var md5 = require('MD5');
var mysql = require('mysql');

// crée un serveur JS
httpServer = http.createServer(function(req, resultat){
	// Affiche un message dans la console
	console.log("Un utilisateur à affiché la page."); 

	// affiche un message sur la page
	resultat.end('Hello World');
});

// Ouvre un port xxxx 
httpServer.listen(1337);

/**
 * Connection BDD 
 **/
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'socketio'
});


// on appel socket io et on relie au serveur JS
var io = require('socket.io').listen(httpServer);

/**
 * OBJETS
 **/
var users = {};
var messages = [];
var history = 3;

/**
 * écouter quand il y a une connection coté server
 **/
io.sockets.on('connection', function(socket){

	// chaque utilisateur aura sa propre socket
	// console.log('Nouveau utilisateur');
	
	// on crée une variable qui va contenir le nom de l'utilisateur émis
	var me = false;

	// on parcours tous les utilisateurs connectés
	for( var k in users ){
		socket.emit('newuser', users[k]);
	}
	// on parcours tous les messages
	for( var m in messages ){
		socket.emit('newmsg', messages[m]);
	}

	/**
	 * on ecoute un evenement login lors d'une connection
	 **/
	socket.on('login', function(user){
		/**
		 * Utilisateur current
		 * Object -> username : mail
		 **/
		me = user; // on recupère l'objet user
		me.pseudo = user.prenom; 
		me.email = user.email;
		me.id = user.email.replace('@','-').replace('.','-');
		me.avatar = 'https://gravatar.com/avatar/' + md5(user.mail) + '?s=50';

		// on envoie un evenement a la socket ACTUEL
		socket.emit('logged'); // cache le formulaire

		// on rajoute l'utilisateur connecté au tableaux d'utilisateurs déjà connecté
		//users[me.id] = me;

		// on envoie un evenement a tous les autres sockets
		// socket.broadcast.emit('newuser');

		// requet user
		// var reqLogin = 'SELECT * FROM users WHERE prenom = "' + me.pseudo + '" AND email = "' + me.email + '"';
		var reqLogin = 'SELECT * FROM users';
		//var reqLogin = "SELECT * FROM users WHERE prenom = " + me.pseudo + " AND email = " + me.email +'"';
		connection.query( reqLogin ,function( error, rows, fields ){
			
			if( error ){ // error
                console.log( error );
                return;
            }

            for( r = 0; r < rows.length; r++){
            	if( rows[r].prenom == me.pseudo && rows[r].email == me.email ){
            		me = rows[r]; // on assigne l'objet au tuple trouvé
            	}
            }

            console.log(me.avatar);

            users[me.id] = me;
            if( me.avatar == undefined){
            	me.avatar = 'https://gravatar.com/avatar/' + md5(user.email) + '?s=50';
            }
    		io.sockets.emit('newuser', me); //  données formulaire

		});
		
	});

	/**
	 * Gestion deconnection
	 **/
	socket.on('disconnect', function(){
		if(!me){
			return false;
		}
		delete users[me.id];
		io.sockets.emit('dissusers', me);
	});


	/**
	 * Reception message
	 **/
	socket.on('newmsg', function(message, user){
		message.user = me;
		date = new Date();
		message.h = date.getHours();
		message.m = date.getMinutes();
		messages.push(message);

		if( messages.length > history ){
			messages.shift();
		}

		io.sockets.emit('newmsg', message);
	});


});