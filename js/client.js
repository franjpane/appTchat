(function($){

	/**
	 * connection de socket io sur notre server
	 **/
	var socket = io.connect('http://localhost:1337');
	var msgtpl = $('#msgtpl').html();
	$('#msgtpl').remove();

	/**
	 * connection bdd
	 **/
	socket.on('connect', function(err){
		if(err){
			console.log('Connection bdd echoué :' + err);
		} else {
			console.log('Connection bdd');
		}
	});

	/**
	 * formulaire de connection
	 **/
	$('#loginform').submit(function(e){
		e.preventDefault();

		// evenement -- function emit -- login = nom d'evenement -- objet
		socket.emit('login', {
			prenom : $('#username').val(),
			email : $('#mail').val()
		});		

		$('#form').removeClass('hide');
		$("#loginform").addClass('hide');
	});

	/**
	 * connecté
	 * On cache le formulaire de connection
	 **/
	socket.on('logged', function(){
		$('#loginform').fadeOut();
	});


	/**
	 * Envoie de message 
	 **/
	$('#form').submit(function(event){
		event.preventDefault();
		socket.emit('newmsg', {
			message: $('#message').val()
		});
		$('#message').val('').focus();
	})

	socket.on('newmsg', function(message, user){
		$('#messages').append('<div class="message">' + Mustache.render(msgtpl, message) + '</div><br><br>' );
		$('#messages').animate({scrollTop : $('#messages').prop('scrollHeight')}, 500);
	});

	/**
	 * Gestion connection
	 **/
	socket.on('newuser', function(user){
		var nom = "";

		$('#users').append('<img src="' + user.avatar + '" id="'+ user.id +'">');
		
		if(user.nom)
			nom = user.nom;

		$('#messages').append('<div class="message"><strong>'+ user.prenom + ' ' + nom + '</strong> s\'est connecté</div><br><br>' );
		$('#messages').animate({scrollTop : $('#messages').prop('scrollHeight')}, 500);
	});

	/**
	 * Gestion déconnection
	 **/
	socket.on('dissusers', function(user){
		var nom = "";

		$('#' + user.id).remove();

		if(user.nom)
			nom = user.nom;
		
		$('#messages').append('<div class="message"><strong>'+ user.prenom + ' ' + nom + '</strong> s\'est déconnecté</div><br><br>' );
		$('#messages').animate({scrollTop : $('#messages').prop('scrollHeight')}, 500);
	});
})(jQuery);